#include <iostream>
#include <boost/array.hpp>
#include <boost/numeric/odeint.hpp>
#include <assert.h>

using namespace std;
using namespace boost::numeric::odeint;



/////////////////////////////////////////////////////////////
double Dt;
double TRUN;
double GAMMA;
double BETA;
double MU;
double NU;

typedef boost::array< double , 3 > state_type;
void sir( const state_type &x , state_type &dxdt , double t )
{
    dxdt[0] = -BETA*x[0]*x[1] - MU*x[0]; 		// dS/dt=-beta*S*I - mu*S
    dxdt[1] =  BETA*x[0]*x[1] - GAMMA*x[1] -MU*x[1]; 	// dI/dt=beta*S*I-gamma*I-mu*I
    dxdt[2] =  GAMMA*x[1] - MU*x[2]; 			// dR/dt = gamma*I - mu*R
}
void write_sir( const state_type &x , const double t )
{
    cout << t << '\t' << x[0] << '\t' << x[1] << '\t' << x[2] << endl;
}


////////////////// Experimentos //////////////////

/*
set xla 't'; 
set yla 'State'; 
plot '< ./a.out 1.0 0.2 0.0 0.0 100' u 1:3 w lp t 'I', '' u 1:2 w lp t 'S', '' u 1:4 w lp t 'R'
*/
int naiveSIR(char **argv){
    std::cout << "#SIR naive" << std::endl;	

    BETA = atof(argv[1]);	
    GAMMA = atof(argv[2]);
    MU = atof(argv[3]);
    NU = atof(argv[4]);
    TRUN = atoi(argv[5]);
    Dt=0.01;	
	
    // initial conditions (S[0],I[0],R[0])
    state_type x = { 1.0 , 0.00001 , 0.0 }; 

    // integracion con tasa de infeccion normal hasta t=taislamiento
    integrate( sir , x , 0.0 , TRUN , Dt , write_sir );

    return 0;
}

int main(int argc, char **argv)
{
    naiveSIR(argv);
    return 0;
}

