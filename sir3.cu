/*
====
Solves the model

S => I1 -> I2 -> ... -> IM -> ... -> IN -> R

dS/dt = -beta*S*InfectedGroup - mu*S
dI_1/dt = beta*S*InfectedGroup - gamma*N*I_1 - mu*I_1
dI_i/dt = gamma*N*I_{i-1} - gamma*N*I_i - mu*I_i
dR/dt = GAMMA*I_N - mu*R

InfectedGroup =  I_{M+1} + I_2 + ... + I_N
ExposedGroup = I_1 + I_2 + ... + I_M

Ref.
Modeling Infectious Diseases IN HUMANS AND ANIMALS
Matt J. Keeling and Pejman Rohani
2008 by Princeton University Press

Ecuaciones del modelo:
3.3.1. SEIR and Multi-Compartment Models, pag 94, eq 3.13

TODO: 
* Desagregar en grados de una red scale-free.
* Desagregar en edades.
*/

#if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
#include "gpu_timer.h"
#endif

#include "cpu_timer.h"


#include <iostream>
#include <fstream>
#include <boost/array.hpp>
#include <assert.h>
#include <cstdlib>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform.h>

#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>
#include <boost/numeric/odeint/integrate/integrate_const.hpp>
#include <boost/numeric/odeint/external/thrust/thrust.hpp>

using namespace std;
using namespace boost::numeric::odeint;

/////////////////////////////////////////////////////////////
double Dt;

//typedef boost::array< double , NN+2 > state_type;
typedef double value_type;
typedef thrust::device_vector<value_type> state_type;
typedef state_type::const_iterator const_it_state_type;
typedef state_type::iterator it_state_type;

ofstream varout("vars.gnu");	
ofstream resout("results.dat");

class sir_system{
	private:
	const_it_state_type S_begin, S_end;		
	const_it_state_type E_begin, E_end;		
	const_it_state_type I_begin, I_end;		
	const_it_state_type R_begin, R_end;		

	int Nn,Mm; 
	value_type Beta, Gamma, Mu, Nu;	
	value_type Trun;
	value_type initialS;

	public:
	sir_system(const state_type &x, int NN_, int MM_, 
	value_type BETA_, value_type GAMMA_, value_type MU_, value_type NU_, value_type TRUN_):
	Nn(NN_),Mm(MM_),Beta(BETA_),Gamma(GAMMA_),Mu(MU_),Nu(NU_),Trun(TRUN_)
	{
		S_begin=x.begin(); S_end=S_begin+1;
		E_begin=S_end; E_end=E_begin+Mm;
		I_begin=E_end; I_end=I_begin+(Nn-Mm);
		R_begin=I_end; R_end=R_begin+1;	
		
		initialS=x[0];	

    		varout << "N=" << Nn << "; M=" << Mm << "; beta=" 
		<< Beta << "; gamma=" << Gamma << "; nu=" 
		<< Nu << "; mu=" << Mu << "; trun=" 
		<< Trun << std::endl;
	}

	void operator()( const state_type &x , state_type &dxdt , const value_type t )
	{
	    // x[0] = S == susceptible
	    // x[1],x[2],...,x[M] = I_1, I_2,..., IM == exposed 
	    // x[M+1],x[2],...,x[N] = I_1, I_2,..., IN == infected
	    // x[N+1]= R == recovered

	    // mapping iterators to classes
	    it_state_type dSdt_begin=dxdt.begin(); it_state_type dSdt_end=dSdt_begin+1;
	    it_state_type dEdt_begin=dSdt_end; it_state_type dEdt_end=dEdt_begin+Mm;
	    it_state_type dIdt_begin=dEdt_end; it_state_type dIdt_end=dIdt_begin+(Nn-Mm);
	    it_state_type dRdt_begin=dIdt_end; it_state_type dRdt_end=dRdt_begin+1;		

	    // InfectedGroup = 	I_{M+1} + ... + I_N = poblaciones contagiosas
	    double InfectedGroup=0.0;
	    InfectedGroup=thrust::reduce(I_begin,I_end);	
	
	    // dS/dt = -beta*S*InfectedGroup - mu*S
	    value_type S=*S_begin;	    		 			
	    *dSdt_begin = -Beta*S*InfectedGroup - Mu*S + Nu*S; 			
	
	    // dI_1/dt = beta*S*InfectedGroup - gamma*N*I_1 - mu*I_1
	    value_type E1=*E_begin;	    		 			
	    *dEdt_begin =  Beta*S*InfectedGroup - Gamma*Nn*E1 -Mu*E1; 	
	
	    // dI_i/dt = gamma*N*I_{i-1} - gamma*N*I_i - mu*I_i
	    using namespace thrust::placeholders;
	    thrust::transform(E_begin+1,I_end,E_begin,dEdt_begin+1,Gamma*Nn*(_2-_1)-Mu*_1);		
	
	    // dR/dt = GAMMA*I_N - mu*R
	    value_type R=*R_begin;	    		 			
	    value_type I_last=*(I_end-1);	    		 			
	    *dRdt_begin = Gamma*Nn*I_last - Mu*R; 				
	}

    	// basic reproduction ratio (valid if trun long enough to reach stationarity)
	value_type R0()
	{
		value_type S=*(S_begin);
		value_type R=*(R_begin);

		value_type R0=-log(S/initialS)/R;
    		varout << "R0 = "<< R0 << std::endl;
		return R0;	
	}
};


struct write_sir_detailed{

	int MM,NN;

	write_sir_detailed(int NN_,int MM_):NN(NN_),MM(MM_){
	};

	void operator()( const state_type &x , const double t)
	{

	    const_it_state_type S_begin=x.begin(); const_it_state_type S_end=S_begin+1;
	    const_it_state_type E_begin=S_end; const_it_state_type E_end=E_begin+MM;
	    const_it_state_type I_begin=E_end; const_it_state_type I_end=I_begin+(NN-MM);
	    const_it_state_type R_begin=I_end; const_it_state_type R_end=R_begin+1;	

    	    double InfectedGroup=0.0;
	    InfectedGroup=thrust::reduce(I_begin,I_end);	

	    double ExposedGroup=0.0;
	    ExposedGroup=thrust::reduce(E_begin,E_end);	

	    resout << t;
	    for(int i=0;i<NN+2;i++) resout  << '\t' << x[i];
	    resout << '\t' << InfectedGroup;
	    resout << '\t' << ExposedGroup << endl;
	}
};

struct write_sir{

	int MM,NN;

	write_sir(int NN_,int MM_):NN(NN_),MM(MM_){
	};

	void operator()( const state_type &x , const double t)
	{

	    const_it_state_type S_begin=x.begin(); const_it_state_type S_end=S_begin+1;
	    const_it_state_type E_begin=S_end; const_it_state_type E_end=E_begin+MM;
	    const_it_state_type I_begin=E_end; const_it_state_type I_end=I_begin+(NN-MM);
	    const_it_state_type R_begin=I_end; const_it_state_type R_end=R_begin+1;	

    	    double InfectedGroup=0.0;
	    InfectedGroup=thrust::reduce(I_begin,I_end);	

	    double ExposedGroup=0.0;
	    ExposedGroup=thrust::reduce(E_begin,E_end);	

	    resout << t;
	    resout  << '\t' << x[0];
	    for(int i=1;i<NN+1;i++) resout  << '\t' << 0.0;
	    resout  << '\t' << x[NN+1];
	    resout << '\t' << InfectedGroup;
	    resout << '\t' << ExposedGroup << endl;
	}
};


int naiveSIR(int NN, int MM, value_type BETA, value_type GAMMA, value_type MU, value_type NU, value_type trun)
{    	
    Dt=0.01;	 
    resout << "# S, E1,..., EM, IM+1,...,IN, R, SumIi, SumEi" << std::endl;		

    // initial conditions (S[0],I[0],R[0])
    state_type x(NN+2,0.0); 
    value_type initialI=0.001;	
    value_type initialS=1-initialI;	
    x[1]=initialI; // == I1 (first exposed class)
    x[0]=initialS; // == S (all susceptibles)

    sir_system sir(x,NN,MM,BETA,GAMMA,MU,NU,trun);

    // integracion 
    runge_kutta4< state_type , value_type , state_type , value_type > stepper;
    integrate_const( stepper, sir , x , 0.0 , trun , Dt , write_sir(NN,MM) );
    //integrate( sir , x , 0.0 , trun , Dt , write_sir(NN,MM) );
 
    std::cout << sir.R0() << std::endl;	

    return 0;
}

int main(int argc, char **argv)
{
    assert(argc==8);
    int NN=atoi(argv[1]);	
    int MM=atoi(argv[2]);	
    value_type BETA=atof(argv[3]);
    value_type GAMMA=atof(argv[4]);
    value_type MU=atof(argv[5]);
    value_type NU=atof(argv[6]);	
    value_type trun=atof(argv[7]);

    cpu_timer Reloj;

    #if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_OMP
    std::cout << "OMP run" << std::endl;	
    Reloj.tic();	
    #elif THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CPP
    std::cout << "CPP run" << std::endl;	
    Reloj.tic();	
    #elif THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
    std::cout << "CUDA run" << std::endl;	
    gpu_timer Reloj_gpu;
    Reloj_gpu.tic();	
    #else	
    #endif	    	
 
    naiveSIR(NN,MM,BETA,GAMMA,MU,NU,trun);    	

    std::cout << "(cpu clock) ms=" << Reloj.tac() << std::endl;

    #if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
    std::cout << "(gpu clock) ms=" << Reloj_gpu.tac() << std::endl;
    #endif
		
    return 0;
}

