if(first==0){
res
BETA_S=1.0
BETA_A=1.0
ALPHA=1.0
GAMMA=0.2
FRAC=0.1
MU=0.0
NU=0.0
NE=1
NI=1
TRUN=100
first=1
system("make sir4cpp")
set term qt 0 size 700,400 title "SE{Is,Ia}R Model"
}

#lista=sprintf("BETA_S %1.2f\nBETA_A %f\nALPHA %f\nGAMMA %f\nFRAC %f\nMU %f\nNU %f\nTRUN %d\ne",\
#BETA_S,BETA_A,ALPHA,GAMMA,FRAC,MU,NU,TRUN);

comando=sprintf("make run4 BETA_S=%f BETA_A=%f ALPHA=%f GAMMA=%f FRAC=%f MU=%f NU=%f NE=%d NI=%d TRUN=%d",\
BETA_S,BETA_A,ALPHA,GAMMA,FRAC,MU,NU,NE,NI,TRUN);

#print comando
system(comando)
load 'vars.gnu'

#titulo=sprintf("BETA\_S=%1.2f BETA\_A=%1.2f ALPHA=%1.2f GAMMA=%1.2f FRAC=%1.2f MU=%1.2f NU=%1.2f TRUN=%d R0=%1.2f",\
#BETA_S,BETA_A,ALPHA,GAMMA,FRAC,MU,NU,TRUN,R0);

titulo=sprintf("{/Symbol b}_s=%1.3f {/Symbol b}_a=%1.3f {/Symbol a}=%1.3f {/Symbol g}=%1.3f FRAC_s=%1.3f {/Symbol m}=%1.3f {/Symbol n}=%1.3f NE=%d NI=%d TRUN=%d R_0=%1.3f",\
BETA_S,BETA_A,ALPHA,GAMMA,FRAC,MU,NU,NE, NI, TRUN,R0);


set tit titulo enhanced
set key autotitle columnhead bot; 
plot [][:] for[i=2:7] 'results.dat' u 1:(column(i)) w l lw (i==7 || i==4)?(3):(1) lc i

pause mouse key

inc=0.02
inc2=inc*0.1

#66 B
#98 b
if(MOUSE_KEY==66){BETA_S=BETA_S+inc; }
if(MOUSE_KEY==98){BETA_S=(BETA_S>inc)?(BETA_S-inc):(BETA_S);}

#86 V
#118 v
if(MOUSE_KEY==86) {BETA_A=BETA_A+inc; }
if(MOUSE_KEY==118) {BETA_A=(BETA_A>inc)?(BETA_A-inc):(BETA_A); }

#65 A
#97 a
if(MOUSE_KEY==65){ALPHA=ALPHA+inc;}
if(MOUSE_KEY==97){ALPHA=(ALPHA>inc)?(ALPHA-inc):(ALPHA);}

#70 F
#102 f
if(MOUSE_KEY==70) {FRAC=(FRAC<1.0-inc2)?(FRAC+inc2):(1.0); }
if(MOUSE_KEY==102) {FRAC=(FRAC>inc2)?(FRAC-inc2):(FRAC); }

#71 G
#103 g
if(MOUSE_KEY==71) {GAMMA=GAMMA+inc; }
if(MOUSE_KEY==103) {GAMMA=(GAMMA>inc)?(GAMMA-inc):(GAMMA);}

#84 T
#116 t
if(MOUSE_KEY==84) {TRUN=TRUN+50; }
if(MOUSE_KEY==116) {TRUN=(TRUN>50)?(TRUN-50):(TRUN); }

#75 K
#107 k
if(MOUSE_KEY==75) {NE=NE+1; }
if(MOUSE_KEY==107) {NE=(NE>1)?(NE-1):(1); }

#76 L
#108 l
if(MOUSE_KEY==76) {NI=NI+1; }
if(MOUSE_KEY==108) {NI=(NI>1)?(NI-1):(1); }


reread

