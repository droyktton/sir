if(first==0){
res

#$(ALPHA) $(BETA_A) $(BETA_S) $(GAMMA) $(MU) $(FRAC) $(FRACM) $(NDEG) $(PIA) $(PIS) $(PS) $(TRUN)

## default
ALPHA=1.0
BETA_S=1.0
BETA_A=1.0
GAMMA=0.2
MU=0.1
FRAC=0.1
FRACM=0.05
NDEG=10
PIA=1
PIS=1
PS=1
TRUN=100

first=1
system("make sir6cpp")
set term qt 0 size 700,400 title "SE{Is,Ia}RM Model with degrees"
}

comando=sprintf("make run6 ALPHA=%f BETA_A=%f BETA_S=%f GAMMA=%f MU=%f FRAC=%f FRACM=%f NDEG=%d \
PIA=%f PIS=%f PS=%f TRUN=%d",ALPHA, BETA_A,BETA_S, GAMMA, MU, FRAC,FRACM,NDEG, PIA, PIS, PS, TRUN);

print comando

system(comando)
load 'vars.gnu'

titulo=sprintf("{/Symbol a}=%1.3f {/Symbol b}_a=%1.3f {/Symbol b}_s=%1.3f {/Symbol g}=%1.3f {/Symbol m}=%1.3f FRAC_s=%1.3f FRACM=%1.3f NDEG=%d P_{Ia}=%f P_{Is}=%f P_S=%f TRUN=%d",\
ALPHA,BETA_A,BETA_S,GAMMA,FRAC,FRACM,MU,NDEG,PIA,PIS,PS,TRUN);

set tit titulo
plot [:TRUN][1e-6:] for[i=2:7] 'results.dat' u 1:(column(i)) t columnhead w lp


