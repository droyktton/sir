/*
====
Solves the model

S => I1 -> I2 -> ... -> IM -> ... -> IN -> R

dS/dt = -beta*S*InfectedGroup - mu*S
dI_1/dt = beta*S*InfectedGroup - gamma*N*I_1 - mu*I_1
dI_i/dt = gamma*N*I_{i-1} - gamma*N*I_i - mu*I_i
dR/dt = GAMMA*I_N - mu*R

InfectedGroup =  I_{M+1} + I_2 + ... + I_N
ExposedGroup = I_1 + I_2 + ... + I_M


Ref.
Modeling Infectious Diseases IN HUMANS AND ANIMALS
Matt J. Keeling and Pejman Rohani
2008 by Princeton University Press

Ecuaciones del modelo:
3.3.1. SEIR and Multi-Compartment Models, pag 94, eq 3.13

TODO: 
* Desagregar en grados de una red scale-free.
* Desagregar en edades.
* Paralelizar usando Thrust.
*/


#include <iostream>
#include <fstream>
#include <boost/array.hpp>
#include <boost/numeric/odeint.hpp>
#include <assert.h>
#include <cstdlib>

using namespace std;
using namespace boost::numeric::odeint;

/////////////////////////////////////////////////////////////
double Dt;

#ifndef NN
#define N	1
//#define NN	2
#endif

#ifndef MM
#define M	0
//#define MM	1
#endif

#ifndef NAGES
#define NAGES	4
#endif

typedef boost::array< double , ((NN+2)*NAGES) > state_type;
typedef boost::array< double , NAGES > parameter_type;

parameter_type Gamma;
parameter_type Beta;
parameter_type Mu;
parameter_type Nu;

void sir( const state_type &x , state_type &dxdt , double t )
{

    // age "a", a=0,1,2,3 (4 ages)
    // off=NAGES*a
    // x[off+0] = S == susceptible
    // x[off+1],x[off+2],...,x[off+M] = I_1, I_2,..., IM == exposed age a
    // x[off+M+1],x[off+2],...,x[off+N] = I_1, I_2,..., IN == infected age a
    // x[off+N+1]= R == recovered age a

    // InfectedGroup = 	I_{M+1} + ... + I_N = poblaciones contagiosas
    double InfectedGroup=0.0;
    for(int a=0;a<NAGES;a++){
	int off=(NN+2)*a;
	
    	for(int i=MM+1;i<=NN;i++){ 
		InfectedGroup+=x[off+i];
	}
    }	

    for(int a=0;a<NAGES;a++){
	int off=(NN+2)*a;	

    	// dS/dt = -beta*S*InfectedGroup - mu*S
    	dxdt[off+0] = -Beta[a]*x[off+0]*InfectedGroup - Mu[a]*x[off+0] + Nu[a]*x[off+0]; 			

    	// dI_1/dt = beta*S*InfectedGroup - gamma*N*I_1 - mu*I_1
    	dxdt[off+1] =  Beta[a]*x[off+0]*InfectedGroup - Gamma[a]*NN*x[off+1] -Nu[a]*x[off+1]; 	

    	// dI_i/dt = gamma*N*I_{i-1} - gamma*N*I_i - mu*I_i
    	for(int i=2;i<=NN;i++)
    	dxdt[off+i] =  Gamma[a]*NN*x[off+i-1] - Gamma[a]*NN*x[off+i] - Mu[a]*x[off+i]; 		

    	// dR/dt = GAMMA*I_N - mu*R
    	dxdt[off+NN+1] = Gamma[a]*NN*x[off+NN] - Mu[a]*x[off+NN+1]; 				
    }	
}

ofstream resout;
void write_sir( const state_type &x , const double t )
{
    resout << t;
    for(int a=0;a<NAGES;a++){
	int off=(NN+2)*a;	
    	for(int i=0;i<NN+2;i++) resout  << '\t' << x[i+off];
    }	
    resout << endl;	
}


int naiveSIR(double trun){

    Dt=0.01;	
 
    resout.open("results.dat");

    // prints columheader titles
    resout << "t";
    for(int a=0;a<NAGES;a++){
    	resout << " S_" << a ;

	for(int e=0;e<MM;e++)
	resout << " E_{" << a << "," << e << "}"; 

	for(int i=0;i<NN-MM;i++)
	resout << " I_{" << a << "," << i << "}"; 

    	resout << " R_" << a ;
    }
    resout << endl;
		
    // gnuplot vars
    ofstream varout("vars.gnu");	
    varout 
	<< "N=" << NN << "; M=" << MM << "; NAGES=" << NAGES
	<< "; beta=" << BETA << "; gamma=" << GAMMA << "; nu=" << NU << "; mu=" << MU 
	<< "; trun=" << trun << std::endl;

    // initial conditions (S[0],I[0],R[0])
    state_type x; 
    
    for(int a=0;a<NAGES;a++){
	int off=(NN+2)*a;
	x[off+0]=1.0;	
    	for(int i=1;i<=NN+1;i++) x[off+i]=0.0; // == I2,...,INN, R (rest of the world)	

	Beta[a]=BETA; Gamma[a]=GAMMA; Nu[a]=(a==0)?(NU):(0.0); Mu[a]=MU;
    }

    double initialI=0.1;	
    double initialS=1-initialI;	
    for(int a=0;a<1;a++){
    	int off=(NN+2)*a;	
    	x[off+1]=initialI; // == I1 (first exposed class, age=0)
    	x[off+0]=initialS; // == S (all susceptibles)
    }		
	
    // integracion 
    integrate( sir , x , 0.0 , trun , Dt , write_sir );

    // basic reproduction ratio (reached if trun long enough)
    double totalS=0.0;	
    double totalR=0.0;	
    for(int a=0;a<NAGES;a++){
	int off=(NN+2)*a;	
	totalS+=x[off+0];
	totalR+=x[off+NN+1];
    }	
    varout << "R0 = "<< -log(totalS/initialS)/totalR << std::endl;	

    return 0;
}

int main(int argc, char **argv)
{
    assert(argc==2);
    double trun=atof(argv[1]);

    naiveSIR(trun);    	

    return 0;
}

